import os
import random
import numpy as np
from tensorflow.contrib.learn.python.learn.datasets import base
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def InputFN(data, label, length):
    resultList = random.sample(range(0, length), batch_size)
    for i in range(batch_size):
        if i == 0:
            datalist = list(training_set.data[resultList[i]])
            labellist = np.array(training_set.target[resultList[i]])
        else:
            datalist.extend(list(training_set.data[resultList[i]]))
            labellist = np.append(labellist, training_set.target[resultList[i]])

    data_tensor = tf.convert_to_tensor(datalist)
    labellist = tf.one_hot(labellist, 3)
    label_tensor = tf.convert_to_tensor(labellist)
    data_batch = tf.reshape(data_tensor, [-1, 4])
    label_batch = tf.reshape(label_tensor, [-1, 3])
    return data_batch, label_batch


# 所用的数据集文件
IRIS_TRAINING = "iris_training(normalize).csv"
IRIS_TEST = "iris_test(normalize).csv"
# 加载数据集
training_set = base.load_csv_with_header(filename=IRIS_TRAINING,
                                         features_dtype=np.float32,
                                         target_dtype=np.int)
test_set = base.load_csv_with_header(filename=IRIS_TEST,
                                     features_dtype=np.float32,
                                     target_dtype=np.int)
# train_data_set = tf.convert_to_tensor(training_set.data)
# print(train_data_set)
# train_label_set = tf.convert_to_tensor(training_set.target)
dataMax = np.amax(training_set.data)
batch_size = 30
train_data_length = 120
test_data_length = 30
minBatch = int(train_data_length / batch_size)
testmin_Batch = int(test_data_length / batch_size)

train_data_batch, train_label_batch = InputFN(training_set.data, training_set.target, train_data_length)
test_data_batch, test_label_batch = InputFN(test_set.data, test_set.target, test_data_length)

learning_rate = 1e-4
Logpath = 'logs/'
epoch = 10000
if not os.path.isdir(Logpath):
    os.mkdir(Logpath)

x = tf.placeholder(tf.float32, [batch_size, 4])
y_ = tf.placeholder(tf.float32, [batch_size, 3])
keep_prob = tf.placeholder(tf.float32)
DroupOutR = 0.7

with tf.name_scope('hidden-1'):
    w_h1 = weight_variable([4, 10])
    b_h1 = bias_variable([10])
    h1 = tf.matmul(x, w_h1) + b_h1
    h1 = tf.nn.relu(h1)
    h1 = tf.nn.dropout(h1, keep_prob)
    # h1 = tf.nn.sigmoid(h1)

with tf.name_scope('hidden-2'):
    w_h2 = weight_variable([10, 10])
    b_h2 = bias_variable([10])
    h2 = tf.matmul(h1, w_h2) + b_h2
    h2 = tf.nn.relu(h2)
    h2 = tf.nn.dropout(h2, keep_prob)
    # h2 = tf.nn.sigmoid(h2)

with tf.name_scope('hidden-3'):
    w_h3 = weight_variable([10, 10])
    b_h3 = bias_variable([10])
    h3 = tf.matmul(h2, w_h3) + b_h3
    h3 = tf.nn.relu(h3)
    h3 = tf.nn.dropout(h3, keep_prob)
    # h3 = tf.nn.sigmoid(h3)

with tf.name_scope('Output'):
    w_op = weight_variable([10, 3])
    b_op = bias_variable([3])
    op = tf.matmul(h3, w_op) + b_op
    Output = tf.nn.softmax(op)

with tf.name_scope('cross_entropy'):
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits=Output, labels=y_)
    cross_entropy = tf.reduce_mean(cross_entropy)

# 定義訓練模型
with tf.name_scope('train'):
    train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)
    # train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(cross_entropy)

with tf.name_scope('accuracy'):
    correct_prediction = tf.equal(tf.argmax(Output, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    writer_1 = tf.summary.FileWriter(Logpath)
    writer_1.add_graph(sess.graph)
    tf.summary.scalar('Loss', cross_entropy)
    tf.summary.scalar('Accuracy', accuracy)
    writer_op = tf.summary.merge_all()
    print('Start ...')
    for i in range(1, epoch + 1):

        if i % 500 == 0:
            summary = sess.run(writer_op, feed_dict={x: train_data, y_: label_data, keep_prob: DroupOutR})
            writer_1.add_summary(summary, i)
            writer_1.flush()
            accCount = 0
            lossCount = 0
            Count = 0
            for j in range(minBatch):
                train_data, label_data = sess.run([train_data_batch, train_label_batch])
                _, train_loss, train_acc = sess.run([train_step, cross_entropy, accuracy],
                                                    feed_dict={x: train_data, y_: label_data, keep_prob: DroupOutR})
                accCount += train_acc
                lossCount += train_loss
                Count += 1
            accCount /= Count
            lossCount /= Count
            test_data, test_label = sess.run([test_data_batch, test_label_batch])
            test_loss, test_acc = sess.run([cross_entropy, accuracy],
                                           feed_dict={x: test_data, y_: test_label, keep_prob: DroupOutR})
            print('epoch %d, train loss : %4.3f, train acc : %4.2f%%' % (i, lossCount, accCount * 100))
            print('        , test loss : %4.3f, test acc : %4.2f%%' % (test_loss, test_acc * 100))
        elif i % 50 == 0:
            train_data, label_data = sess.run([train_data_batch, train_label_batch])
            _, train_loss = sess.run([train_step, cross_entropy],
                                     feed_dict={x: train_data, y_: label_data, keep_prob: DroupOutR})
            print('epoch %d, loss : %4.3f' % (i, train_loss))
        else:
            for j in range(minBatch):
                train_data, label_data = sess.run([train_data_batch, train_label_batch])
                sess.run(train_step, feed_dict={x: train_data, y_: label_data, keep_prob: DroupOutR})

print('Done !')
