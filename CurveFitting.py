import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# 權重
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)

# 偏權值
def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

# 建立資料集
x_data = np.linspace(-1, 1, 300)[:, np.newaxis]
noise = np.random.normal(0, 0.05, x_data.shape)
y_data = np.square(x_data) - 0.5 + noise

print(y_data)

xs = tf.placeholder(tf.float32, [None, 1])
ys = tf.placeholder(tf.float32, [None, 1])

# 輸入層
with tf.name_scope('Input-layer'):


# 輸出層
with tf.name_scope('Output-layer'):


# 損失函數
with tf.name_scope('Loss-function'):

# 最佳化函數
with tf.name_scope('Optimize-function'):
    

init = tf.global_variables_initializer()

with tf.Session() as sess:
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(x_data, y_data)
    plt.show(block=False)

    sess.run(init)

    for train in range(500+1):

        sess.run(train_step, feed_dict={xs: x_data, ys: y_data})
        prediction_value = sess.run(y_, feed_dict={xs: x_data})
        lines = ax.plot(x_data, prediction_value, 'r-', lw=5)
        plt.pause(0.1)
        try:
            ax.lines.remove(lines[0])
        except Exception:
            pass
        if train % 50 == 0:
            print(train, sess.run(loss, feed_dict={xs: x_data, ys: y_data}))
