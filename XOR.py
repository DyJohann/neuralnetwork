import tensorflow as tf
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


# 權重
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


# 偏權值
def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


XOR_X = [[0, 0], [0, 1], [1, 0], [1, 1]]
XOR_Y = [[0], [1], [1], [0]]

# define x,y_


with tf.name_scope('Input-layer'):


with tf.name_scope('hidden-1'):


# 損失函數
with tf.name_scope('Loss-function'):
    loss = tf.reduce_mean(tf.reduce_sum(tf.square(yo - y_), reduction_indices=[1]))
# 最佳化函數
with tf.name_scope('Optimize-function'):
    train_step = tf.train.AdamOptimizer(0.01).minimize(loss)

Logpath = 'XOR-logs'
if not os.path.isdir(Logpath):
    os.mkdir(Logpath)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    writer_1 = tf.summary.FileWriter(Logpath)
    writer_1.add_graph(sess.graph)
    tf.summary.scalar('Loss', loss)
    writer_op = tf.summary.merge_all()

    for i in range(500 + 1):
        sess.run(train_step, feed_dict={x: XOR_X, y_: XOR_Y})
        if i % 10 == 0:
            train_loss = sess.run(loss, feed_dict={x: XOR_X, y_: XOR_Y})
            print('epoch : %d, loss : %4.3f' % (i, train_loss))
            prediction_value = sess.run(yo, feed_dict={x: XOR_X})
            print(prediction_value)
            summary = sess.run(writer_op, feed_dict={x: XOR_X, y_: XOR_Y, })
            writer_1.add_summary(summary, i)
            writer_1.flush()

    print('Done !')
